# Chapter 5

Testing out equations

{$$}
\left|\sum_{i=1}^n a_ib_i\right|
\le
\left(\sum_{i=1}^n a_i^2\right)^{1/2}
\left(\sum_{i=1}^n b_i^2\right)^{1/2}
{/$$}

The four kinematics equations are {$$}d = v_i t + \frac{1}{2} a t^2{/$$},
{$$}v_f^2 = v_i^2 + 2 a d{/$$}, {$$}v_f = v_i + a t{/$$} and {$$}d =
\frac{1}{2}(v_i + v_f) t{/$$}.